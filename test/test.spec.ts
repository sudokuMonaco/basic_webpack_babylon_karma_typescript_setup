import { Setup } from '../src/setup'

describe("Setup", function () {
    it("creates a babylon scene with basic objects", function () {
        const setup = new Setup();
        const scene = setup.createScene();
        setup.addCamera(scene);
        setup.addDefaultLights(scene);
        setup.addTestObject(scene);
        setup.startEngine(scene);
        expect(scene.lights.length).toEqual(2);
        expect(scene.meshes.length).toEqual(1);
        console.log(scene.getEngine());
    });
});
