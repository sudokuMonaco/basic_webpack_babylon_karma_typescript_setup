const webpackConfig = require('./webpack.config.js')

module.exports = function (config) {
  config.set({
    basePath: '',
    exclude: [],
    files: [
      { pattern: 'src/*.ts', watched: true },
      { pattern: 'test/*.ts', watched: true, served: true, included: true },
    ],
    autoWatch: true,
    singleRun: false,
    failOnEmptyTestSuite: false,
    logLevel: config.LOG_WARN,
    frameworks: ['jasmine', 'karma-typescript'],
    preprocessors: {
      '**/*.ts': ['karma-typescript']
    },
    karmaTypescriptConfig: {
      bundlerOptions: {
        transforms: [
          require("karma-typescript-es6-transform")()
        ]
      }
    },
    webpackMiddleware: {
      noInfo: false,
      stats: 'errors-only'
    },
    browsers: ['Chrome'],
    reporters: ['mocha', 'kjhtml'],
    listenAddress: '0.0.0.0',
    hostname: 'localhost',
    port: 9876,
    retryLimit: 0,
    browserDisconnectTimeout: 5000,
    browserNoActivityTimeout: 10000,
    captureTimeout: 60000,
    client: {
      captureConsole: false,
      clearContext: false,
      runInParent: false,
      useIframe: true,
      jasmine: {
        random: false
      }
    },
    webpack: webpackConfig,
    mochaReporter: {
      output: 'noFailures'
    }
  });
};
