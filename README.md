# install dependencies

```
npm install
```

# run application
```
npm start
```
runs on localhost:9000

# run tests
```
npm test
```
