import { Setup } from './setup';

const setup = new Setup();
const scene = setup.createScene();
setup.addCamera(scene);
setup.addDefaultLights(scene);
setup.addTestObject(scene);
setup.startEngine(scene);
