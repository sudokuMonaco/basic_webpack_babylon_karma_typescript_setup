import {
    Scene,
    Engine,
    ArcRotateCamera,
    Vector3,
    HemisphericLight,
    PointLight,
    NullEngine,
    MeshBuilder,
    Mesh
} from '@babylonjs/core';

export class Setup {

    createScene(): Scene {
        let engine: Engine;
        let canvas;
        if (process.env.NODE_ENV === 'development') {
            canvas = document.getElementById('webglCanvas') as HTMLCanvasElement;
            engine = new Engine(canvas, true);
            engine.enableOfflineSupport = false;
        } else {
            engine = new NullEngine();
        }
        return new Scene(engine);
    }

    addCamera(scene: Scene): ArcRotateCamera {
        const camera = new ArcRotateCamera(
            'Camera',
            Math.PI / 2,
            Math.PI / 2,
            2,
            new Vector3(0, 0, 0),
            scene
        );
        if (process.env.NODE_ENV === 'development') {
            camera.attachControl(scene.getEngine().getRenderingCanvas(), true);
        }
        return camera;
    }

    addDefaultLights(scene: Scene) {
        new HemisphericLight('light1', new Vector3(1, 1, 0), scene);
        new PointLight('light2', new Vector3(0, 1, -1), scene);
    }

    addTestObject(scene: Scene): Mesh {
        return MeshBuilder.CreateBox('box', { height: 5 }, scene);

    }

    startEngine(scene: Scene) {
        window.addEventListener('resize', () => {
            scene.getEngine().resize();
        });

        scene.getEngine().runRenderLoop(() => {
            scene.render();
        });
    }
}
